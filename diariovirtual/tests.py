# -*- coding: utf-8 -*-

from djangoplus.admin.models import User
from djangoplus.test import TestCase
from django.conf import settings


class AppTestCase(TestCase):

    def test_app(self):

        User.objects.create_superuser('admin', None, settings.DEFAULT_PASSWORD)

        self.login('admin', settings.DEFAULT_PASSWORD)
