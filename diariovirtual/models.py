# -*- coding: utf-8 -*-
from django.db import transaction
from djangoplus.admin.models import Unit, Organization
from djangoplus.db import models
from django.core.exceptions import ValidationError
from djangoplus.decorators import meta, action, subset, role
from enderecos.models import Endereco
from pessoas.models import PessoaFisicaAbstrata


class Ano(models.Model):

    ano = models.IntegerField(verbose_name='Ano')

    class Meta:
        verbose_name = 'Ano'
        verbose_name_plural = 'Anos'
        menu = 'Anos', 'fa-calendar-plus-o'

    def __str__(self):
        return '{}'.format(self.ano)


class Rede(Organization):

    nome = models.CharField('nome')

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome',)}),
        ('Administração::Administradores', {'relations': ('administrador_set',)}),
        ('Unidades::Escolas', {'relations': ('escola_set',)}),
    )

    class Meta:
        verbose_name = 'Rede'
        verbose_name_plural = 'Redes'
        can_list = 'Administrador'
        menu = 'Redes', 'fa-link'
        list_menu = 'Superusuário'
        list_shortcut = 'Superusuário'
        icon = 'fa-link'
        usecase = 1

    def __str__(self):
        return self.nome


@role('cpf', scope='rede')
class Administrador(PessoaFisicaAbstrata):
    rede = models.ForeignKey(Rede, verbose_name='Rede', composition=True)

    fieldsets = (('Dados Gerais', {'fields': ('rede',)}),) + PessoaFisicaAbstrata.fieldsets

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        usecase = 2


class Escola(Unit):
    rede = models.ForeignKey(Rede, verbose_name='Rede')
    nome = models.CharField('Nome')
    dired = models.CharField('DIRED')
    endereco = models.OneToOneField(Endereco, verbose_name='Endereço', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('rede', 'nome', 'dired', 'endereco')}),
        ('Coordenadores', {'relations': ('coordenador_set',)}),
    )

    class Meta:
        verbose_name = 'Ecola'
        verbose_name_plural = 'Ecolas'
        icon = 'fa-building'
        can_admin_by_organization = 'Administrador'
        can_list_by_unit = 'Coordenador'
        usecase = 3
        list_shortcut = 'Administrador'
        list_display = 'rede', 'nome', 'endereco', 'get_coordenadores'

    def __str__(self):
        return self.nome

    @meta('Coordenadores')
    def get_coordenadores(self):
        return self.coordenador_set.all()


class EtapaEnsino(models.Model):

    descricao = models.CharField('Descrição')
    serie_inicial = models.IntegerField(verbose_name='Ano/Série Inicial', default=1)
    serie_final = models.IntegerField(verbose_name='Ano/Série Final')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', ('serie_inicial', 'serie_final'))}),
        ('Disciplinas', {'relations': ('disciplina_set',)}),
    )

    class Meta:
        verbose_name = 'Etapa de Ensino'
        verbose_name_plural = 'Etapa de Ensinos'
        menu = 'Etapa de Ensinos', 'fa-list-ul'

    def __str__(self):
        return self.descricao


class Disciplina(models.Model):
    etapa_ensino = models.ForeignKey(EtapaEnsino, verbose_name='Etapa de Ensino')
    nome = models.CharField('Nome')
    qtd_dias_letivos = models.IntegerField('Quantidade de Dias Letivos')
    obrigatorio = models.BooleanField(verbose_name='Obrigatório', default=True)

    class Meta:
        verbose_name = 'Disciplina'
        verbose_name_plural = 'Disciplinas'
        usecase = 5

    def __str__(self):
        return self.nome


class Matriz(models.Model):
    descricao = models.CharField(verbose_name='Descrição', search=True)
    escola = models.ForeignKey(Escola, verbose_name='Escola', filter=True, lazy=False)
    etapa_ensino = models.ForeignKey(EtapaEnsino, verbose_name='Etapa de Ensino', filter=True, lazy=False)
    ativa = models.BooleanField(verbose_name='Ativa', default=True, filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', ('escola', 'etapa_ensino'), 'ativa')}),
        ('Disciplinas', {'relations': ('disciplinacurricular_set',)}),
    )

    class Meta:
        verbose_name = 'Matriz Curricular'
        verbose_name_plural = 'Matrizes Curriculares'
        menu = 'Matrizes Curriculares', 'fa-th'
        list_menu = 'Superusuário'
        can_admin_by_organization = 'Administrador'
        add_message = 'Edite os componentes caso seja necessário'
        list_shortcut = 'Administrador'
        icon = 'fa-th'

    def __str__(self):
        return self.descricao

    def save(self, *args, **kwargs):
        pk = self.pk
        super(Matriz, self).save(*args, **kwargs)
        if not pk:
            for serie in range(self.etapa_ensino.serie_inicial, self.etapa_ensino.serie_final+1):
                for disciplina in self.etapa_ensino.disciplina_set.all():
                    DisciplinaCurricular.objects.create(
                        matriz=self, disciplina=disciplina,
                        qtd_dias_letivos=disciplina.qtd_dias_letivos,
                        obrigatorio=disciplina.obrigatorio,
                        serie=serie
                    )


class DisciplinaCurricular(models.Model):
    matriz = models.ForeignKey(Matriz, verbose_name='Matriz', filter=True, lazy=False)
    disciplina = models.ForeignKey(Disciplina, verbose_name='Disciplina', filter=True, lazy=False)
    serie = models.IntegerField(verbose_name='Ano/Série', choices=[[x, x] for x in range(1, 6)], filter=True)
    qtd_dias_letivos = models.IntegerField('Quantidade de Dias Letivos')
    obrigatorio = models.BooleanField(verbose_name='Obrigatório', default=True)

    class Meta:
        verbose_name = 'Disciplina'
        verbose_name_plural = 'Disciplinas'
        can_admin_by_organization = 'Administrador'
        list_lookups = 'matriz__escola'

    def __str__(self):
        return self.disciplina.nome


@role('cpf', scope='escola')
class Coordenador(PessoaFisicaAbstrata):
    escola = models.ForeignKey(Escola, verbose_name='Ecola', composition=True)

    fieldsets = (('Dados Gerais', {'fields': ('escola',)}),) + PessoaFisicaAbstrata.fieldsets

    class Meta:
        verbose_name = 'Coordenador'
        verbose_name_plural = 'Coordenadores'
        can_admin_by_organization = 'Administrador'
        usecase = 4


@role('cpf', scope='escolas')
class Professor(PessoaFisicaAbstrata):
    foto = models.ImageField(verbose_name='Foto', null=True, upload_to='professores', default='/static/images/user.png')
    escolas = models.ManyToManyField(Escola, verbose_name='Escolas', lazy=False)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', ('cpf', 'data_nascimento'), 'endereco'), 'image':'foto'}),
        ('Contatos', {'fields': (('telefone', 'email'),)}),
        ('Escolas', {'fields': ('escolas',)}),
    )

    class Meta:
        verbose_name = 'Professor'
        verbose_name_plural = 'Professores'
        can_admin_by_organization = 'Coordenador'
        menu = 'Professores', 'fa-graduation-cap'
        usecase = 6
        list_shortcut = True
        icon = 'fa-graduation-cap'
        list_display = 'foto', 'nome', 'telefone', 'email'
        list_template = 'image_rows.html'


class Turma(models.Model):
    descricao = models.CharField(verbose_name='Descrição', exclude=True, null=True, search=True)
    escola = models.ForeignKey(Escola, verbose_name='Ecola')
    ano_letivo = models.ForeignKey(Ano, verbose_name='Ano Letivo', filter=True)
    etapa_ensino = models.ForeignKey(EtapaEnsino, verbose_name='Etapa de Ensino', filter=True)
    serie = models.IntegerField(verbose_name='Série/Ano', filter=True, choices=[[x, x] for x in range(1,10)])
    turno = models.CharField(verbose_name='Turno', choices=[['MANHÃ', 'MANHÃ'], ['TARDE', 'TARDE'], ['NOITE', 'NOITE']], filter=True)
    professor = models.ForeignKey(Professor, verbose_name='Professor', null=True, blank=True, help_text='Informe o professor apenas quando ele lecionará todos os diários da turma.', exclude=True)
    sequencial = models.CharField(verbose_name='Sequencial', null=True, blank=True, choices=[['A', 'A'], ['B', 'B'], ['C', 'C']], exclude=True, help_text='Obrigatório quando existe mais uma uma turma no mesmo ano e etapa de ensino no mesmo turno.')

    fieldsets = (
        ('Dados Gerais', {'fields': ('escola', ('ano_letivo', 'etapa_ensino'), ('serie', 'turno'), 'sequencial')}),
        ('Professor', {'fields': ('professor',)}),
        ('Diários::Diários', {'relations': ('diario_set',)}),
        ('Alunos::Alunos', {'relations': ('matriculaturma_set',)}),
    )

    class Meta:
        verbose_name = 'Turma'
        verbose_name_plural = 'Turmas'
        menu = 'Turmas', 'fa-object-group'
        can_admin_by_unit = 'Coordenador'
        usecase = 8
        list_shortcut = True
        icon = 'fa-object-group'
        add_message = 'Turma cadastrada com sucesso'

    def __str__(self):
        return self.descricao

    @transaction.atomic()
    def save(self, *args, **kwargs):
        pk = self.pk
        i = Turma.objects.filter(
            escola=self.escola, ano_letivo=self.ano_letivo,
            etapa_ensino=self.etapa_ensino, serie=self.serie, turno=self.turno
        ).count()
        self.sequencial = 'ABCDEF'[i]
        self.descricao = '{}º ANO {} - {} ({})'.format(self.serie, self.sequencial, self.etapa_ensino, self.turno)
        super(Turma, self).save(*args, **kwargs)
        if not pk:
            qs_matriz = Matriz.objects.filter(escola=self.escola, etapa_ensino=self.etapa_ensino, ativa=True)
            if qs_matriz.count() == 0:
                raise ValidationError('Não existe matriz ativa para {} na escola {}'.format(
                    self.etapa_ensino, self.escola))
            if qs_matriz.count() > 1:
                raise ValidationError('Não existe mais de uma matriz ativa para {} na escola {}'.format(
                    self.etapa_ensino, self.escola))
            for disciplina_curricular in qs_matriz.first().disciplinacurricular_set.filter(serie=self.serie):
                dados = dict(escola=self.escola, turma=self, disciplina_curricular=disciplina_curricular)
                qs = Diario.objects.filter(**dados)
                if not qs.exists():
                    Diario.objects.create(**dados)

    @action(u'Definir Professor')
    def definir_professor(self, professor):
        for diario in self.diario_set.all():
            diario.professor = professor
            diario.save()


class Aluno(PessoaFisicaAbstrata):
    foto = models.ImageField(verbose_name='Foto', null=True, upload_to='alunos', default='/static/images/user.png')
    escola = models.ForeignKey(Escola, verbose_name='Ecola Atual')
    turma_atual = models.ForeignKey(Turma, verbose_name='Turma Atual', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', ('cpf', 'data_nascimento'), 'escola', 'turma_atual'), 'image': 'foto'}),
        ('Dados Pessoais::Endereço', {'fields': ('endereco',)}),
        ('Dados Pessoais::Contatos', {'fields': (('telefone', 'email'),)}),
        ('Dados Escolares::Matrículas em Turmas', {'relations': ('matriculaturma_set',)}),
        ('Histórico Escolar::Históricos', {'relations': ('historico_set__registrohistorico_set',)}),
    )

    class Meta:
        verbose_name = 'Aluno'
        verbose_name_plural = 'Alunos'
        menu = 'Alunos', 'fa-users'
        can_admin_by_unit = 'Coordenador'
        usecase = 7
        list_shortcut = True
        icon = 'fa-users'
        list_display = 'foto', 'nome', 'turma_atual', 'telefone', 'email'
        list_template = 'image_rows.html'
        
    def save(self, *args, **kwargs):
        PessoaFisicaAbstrata.save(self, *args, **kwargs)
        if self.turma_atual and not MatriculaTurma.objects.filter(aluno=self, turma=self.turma_atual):
            MatriculaTurma(aluno=self, turma=self.turma_atual).save()


class MatriculaTurma(models.Model):
    turma = models.ForeignKey(Turma, verbose_name='Turma')
    aluno = models.ForeignKey(Aluno, verbose_name='Aluno')

    class Meta:
        verbose_name = 'Matrícula em Turma'
        verbose_name_plural = 'Matrículas em Turmas'
        can_add_by_unit = 'Coordenador'
        can_delete = 'Coordenador'
        can_list_by_unit = 'Professor'
        list_lookups = 'turma__escola'
        add_label = 'Matricular Aluno'
        usecase = 10

    def __str__(self):
        return 'Vínculo do aluno {} na turma {}'.format(self.aluno, self.turma)

    def save(self, *args, **kwargs):
        super(MatriculaTurma, self).save(*args, **kwargs)
        for diario in self.turma.diario_set.all():
            qs = MatriculaDiario.objects.filter(diario=diario, aluno=self.aluno)
            if not qs.exists():
                MatriculaDiario.objects.create(diario=diario, aluno=self.aluno)


class DiarioManager(models.DefaultManager):

    @subset('Meus Diários', can_view='Professor', dashboard='center')
    def meus_diarios(self, professor: Professor):
        return self.filter(professor=professor)


class Diario(models.Model):
    escola = models.ForeignKey(Escola, verbose_name='Ecola')
    turma = models.ForeignKey(Turma, verbose_name='Turma', composition=True, filter=True)
    disciplina_curricular = models.ForeignKey(DisciplinaCurricular, verbose_name='Disciplina')
    professor = models.ForeignKey(Professor, verbose_name='Professor', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('escola', 'turma', 'disciplina_curricular',)}),
        ('Professor', {'fields': ('professor',)}),
        ('Alunos::', {'relations': ('matriculadiario_set',)}),
        ('Aulas::', {'relations': ('aula_set',)}),
    )

    class Meta:
        verbose_name = 'Diário'
        verbose_name_plural = 'Diários'
        menu = 'Diários', 'fa-book'
        can_admin_by_unit = 'Coordenador'
        can_list_by_role = 'Professor'
        list_shortcut = True
        icon = 'fa-book'
        list_display = 'turma', 'disciplina_curricular', 'professor', 'get_qtd_alunos', 'get_percentual_lancamento_notas', 'get_percentual_lancamento_frequencia'
        usecase = 9
        list_lookups = 'escola'

    def __str__(self):
        return '{} {}'.format(self.disciplina_curricular, self.turma)

    @action(u'Definir Professor', condition='not professor', inline=True)
    def definir_professor(self, professor):
        self.professor = professor
        self.save()

    @action(u'Trocar Professor', condition='professor')
    def trocar_professor(self, professor):
        self.professor = professor
        self.save()

    @meta('Qtd. de Alunos')
    def get_qtd_alunos(self):
        return self.matriculadiario_set.count()

    @meta('% Lançamento de Notas', formatter='progress')
    def get_percentual_lancamento_notas(self):
        lancados = self.matriculadiario_set.exclude(situacao='Cursando').count()
        if lancados:
            total = self.matriculadiario_set.count()
            return int(100.0*lancados/total)
        return 0

    @meta('% Lançamento de Frequência', formatter='progress')
    def get_percentual_lancamento_frequencia(self):
        lancados = self.aula_set.count()
        if lancados:
            total = self.disciplina_curricular.qtd_dias_letivos
            return int(100.0 * lancados / total)
        return 0

    @action('Lançar Notas', style='blank', usecase=10, inline=True, category='Notas', condition='professor')
    def lancar_notas(self):
        matriculas_diario = []
        for matricula_diario in self.matriculadiario_set.all():
            matriculas_diario.append(matricula_diario)
        for i in range(len(matriculas_diario), 40):
            matriculas_diario.append(None)
        return locals()

    @action('Registrar Frequência', style='blank', usecase=10, inline=True, category='Frequência', condition='professor')
    def registrar_frequencias(self):
        aulas_lancadas = dict()
        for aula in self.aula_set.all():
            aulas_lancadas[aula.dia_letivo] = aula
        aulas = []
        matriculas_diario = []
        for i in range(1, self.disciplina_curricular.qtd_dias_letivos + 1):
            if i in aulas_lancadas:
                aula = aulas_lancadas[i]
                aula.alunos_ausentes = aula.falta_set.values_list('matricula_diario', flat=True)
                aulas.append(aula)
            else:
                aulas.append(Aula(dia_letivo=i))
        for matricula_diario in self.matriculadiario_set.all():
            matriculas_diario.append(matricula_diario)
        for i in range(len(matriculas_diario), 40):
            matriculas_diario.append(None)
        dias = range(1, 32)
        meses = ('JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ')
        return locals()


class Aula(models.Model):
    diario = models.ForeignKey(Diario, verbose_name='Diário', composition=True)
    data = models.DateField(verbose_name='Data')
    dia_letivo = models.IntegerField(verbose_name='Dia Letivo')
    conteudo = models.TextField(verbose_name='Conteúdo', default='')

    class Meta:
        verbose_name = 'Aula'
        verbose_name_plural = 'Aulas'
        can_list_by_unit = 'Professor', 'Coordenador'
        list_display = 'data', 'conteudo'
        list_lookups = 'diario__turma__escola'


class MatriculaDiario(models.Model):
    diario = models.ForeignKey(Diario, verbose_name='Diário')
    aluno = models.ForeignKey(Aluno, verbose_name='Aluno')

    n1 = models.IntegerField(verbose_name='N1', null=True, blank=True, exclude=True)
    n2 = models.IntegerField(verbose_name='N2', null=True, blank=True, exclude=True)
    n3 = models.IntegerField(verbose_name='N3', null=True, blank=True, exclude=True)
    n4 = models.IntegerField(verbose_name='N4', null=True, blank=True, exclude=True)
    nr = models.IntegerField(verbose_name='NR', null=True, blank=True, exclude=True)

    situacao = models.CharField(verbose_name='Situação', default='Cursando', exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('diario', 'aluno')}),
    )

    class Meta:
        verbose_name = 'Aluno'
        verbose_name_plural = 'Alunos'
        can_add_by_unit = 'Coordenador'
        can_delete = 'Coordenador'
        can_list_by_unit = 'Professor'
        list_lookups = 'diario__turma__escola'
        add_label = 'Matricular Aluno'
        list_display = 'aluno', 'n1', 'n2', 'n3', 'n4', 'get_media_disciplina', 'nr', 'get_media_final_disciplina', 'situacao'

    def notas_foram_lancadas(self):
        return self.n1 is not None and self.n2 is not None and self.n3 is not None and self.n4 is not None

    @meta('Média')
    def get_media_disciplina(self):
        if self.notas_foram_lancadas():
            return (self.n1 + self.n2 + self.n3 + self.n4) / 4
        return None

    def is_em_recuperacao(self):
        media = self.get_media_disciplina()
        if media is None:
            return False
        return media < 70

    @meta('Nota Final')
    def get_media_final_disciplina(self):
        if self.is_em_recuperacao():
            return self.nr
        else:
            return self.get_media_disciplina()

    def save(self, *args, **kwargs):
        if self.notas_foram_lancadas():
            if self.is_em_recuperacao() and not self.nr:
                self.situacao = 'Recuperação'
            else:
                media_final_disciplina = self.get_media_final_disciplina()
                if media_final_disciplina is not None:
                    if media_final_disciplina >= 70:
                        self.situacao = 'Aprovado'
                    else:
                        self.situacao = 'Reprovado'
                else:
                    self.situacao = 'Cursando'
        else:
            self.situacao = 'Cursando'
        super(MatriculaDiario, self).save(*args, **kwargs)

    def get_qtd_faltas(self):
        return self.falta_set.count()

    def __str__(self):
        return 'Vínculo do aluno {} no diário {}'.format(self.aluno, self.diario)


class Falta(models.Model):
    aula = models.ForeignKey(Aula, verbose_name='Diário')
    matricula_diario = models.ForeignKey(MatriculaDiario, verbose_name='Aluno')

    class Meta:
        verbose_name = 'Frequência'
        verbose_name_plural = 'Frequências'


class Historico(models.Model):
    aluno = models.ForeignKey(Aluno, verbose_name='Aluno', lazy=True)
    escola = models.ForeignKey(Escola, verbose_name='Escola', filter=True, lazy=False)
    etapa_ensino = models.ForeignKey(EtapaEnsino, verbose_name='Etapa de Ensino', filter=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('aluno', ('escola', 'etapa_ensino'))}),
        ('Registros', {'relations': ('registrohistorico_set',)}),
    )

    class Meta:
        verbose_name = 'Histórico'
        verbose_name_plural = 'Históricos'
        can_admin_by_organization = 'Coordenador'
        list_display = 'aluno', 'get_escola', 'etapa_ensino'

    def __str__(self):
        return '{} - {}'.format(self.escola, self.etapa_ensino)

    @meta('Escola')
    def get_escola(self):
        return self.escola


class RegistroHistorico(models.Model):
    historico = models.ForeignKey(Historico, verbose_name='Histórico', lazy=True)
    ano_letivo = models.ForeignKey(Ano, verbose_name='Ano Letivo')
    ano_matriz = models.IntegerField(verbose_name='Ano/Série')
    disciplina = models.ForeignKey(Disciplina, verbose_name='Disciplina')
    qtd_dias_letivos = models.IntegerField('Quantidade de Dias Letivos')
    nota = models.IntegerField(verbose_name='Nota')
    qtd_faltas = models.IntegerField(verbose_name='Faltas')

    matricula_diario = models.ForeignKey(MatriculaDiario, verbose_name='Matrícula Diário', exclude=True, null=True)

    fieldsets = (
        (u'Dados Gerais', {'fields': (('historico', ('ano_letivo', 'ano_matriz')), ('disciplina', 'qtd_dias_letivos'), ('nota', 'qtd_faltas'))}),
    )

    class Meta:
        verbose_name = 'Registro'
        verbose_name_plural = 'Registros'
        can_admin_by_organization = 'Coordenador'
        list_lookups = 'historico__aluno__escola'

    def __str__(self):
        return 'Registro de Histórico {}'.format(self.pk)
