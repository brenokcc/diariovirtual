# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2018-01-29 07:37


from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('diariovirtual', '0017_auto_20180129_0645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='turma',
            name='serie',
            field=djangoplus.db.models.fields.IntegerField(verbose_name='S\xe9rie/Ano'),
        ),
        migrations.AlterField(
            model_name='turma',
            name='turno',
            field=djangoplus.db.models.fields.CharField(choices=[['MANH\xc3', 'MANH\xc3'], ['TARDE', 'TARDE'], ['NOITE', 'NOITE']], max_length=255, verbose_name='Turno'),
        ),
    ]
