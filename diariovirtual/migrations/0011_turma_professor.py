# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2018-01-28 16:31


from django.db import migrations
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('diariovirtual', '0010_auto_20180128_1508'),
    ]

    operations = [
        migrations.AddField(
            model_name='turma',
            name='professor',
            field=djangoplus.db.models.fields.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='diariovirtual.Professor', verbose_name='Professor'),
        ),
    ]
