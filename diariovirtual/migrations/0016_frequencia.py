# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2018-01-28 22:38


from django.db import migrations, models
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('diariovirtual', '0015_auto_20180128_2144'),
    ]

    operations = [
        migrations.CreateModel(
            name='Frequencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('aula', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diariovirtual.Aula', verbose_name='Di\xe1rio')),
                ('matricula_diario', djangoplus.db.models.fields.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='diariovirtual.MatriculaDiario', verbose_name='Aluno')),
            ],
            options={
                'verbose_name': 'Frequ\xeancia',
                'verbose_name_plural': 'Frequ\xeancias',
            },
        ),
    ]
