# -*- coding: utf-8 -*-

# Generated by Django 1.11 on 2018-01-28 07:46


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('diariovirtual', '0002_auto_20180128_0743'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='instituicao',
            options={'verbose_name': 'Institui\xe7\xe3o de Ensino', 'verbose_name_plural': 'Institui\xe7\xe3os de Ensino'},
        ),
    ]
