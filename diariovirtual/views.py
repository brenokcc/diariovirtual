# -*- coding: utf-8 -*-

import datetime
from django.http.response import HttpResponse
from djangoplus.ui.components.paginator import Paginator

from diariovirtual.models import *
from diariovirtual.forms import *
from djangoplus.decorators.views import view, action, dashboard


def lancar_nota(request, dados):
    pk, n, nota = dados.split('::')
    nota = nota and int(nota) or None
    matricula_diario = MatriculaDiario.objects.get(pk=pk)
    setattr(matricula_diario, 'n{}'.format(n), nota)
    matricula_diario.save()
    s = '{}::{}::{}'.format(matricula_diario.get_media_disciplina() or '', matricula_diario.get_media_final_disciplina() or '', matricula_diario.situacao)
    return HttpResponse(s)


def lancar_aula(request):
    dados = request.GET.get('dados')
    pk, dia_letivo, dia, mes, conteudo = dados.split('::')
    diario = Diario.objects.get(pk=pk)
    data = datetime.date(diario.turma.ano_letivo, int(mes), int(dia))
    qs = Aula.objects.filter(diario=diario, dia_letivo=dia_letivo)
    if qs.exists():
        aula = qs[0]
        aula.data = data
        if conteudo:
            aula.coteudo = conteudo
        aula.save()
    else:
        aula = Aula.objects.create(diario=diario, dia_letivo=dia_letivo, data=data, conteudo=conteudo)
    return HttpResponse(aula.pk)


def registrar_frequencia(request):
    dados = request.GET.get('dados')
    print(dados)
    pk, dia_letivo, matricula_diario_id, presente = dados.split('::')
    aula = Aula.objects.get(diario=pk, dia_letivo=dia_letivo)
    matricula_diario = MatriculaDiario.objects.get(pk=matricula_diario_id)
    if presente:
        Falta.objects.get_or_create(aula=aula, matricula_diario=matricula_diario)
    else:
        Falta.objects.filter(aula=aula, matricula_diario=matricula_diario).delete()
    return HttpResponse(matricula_diario.get_qtd_faltas())
