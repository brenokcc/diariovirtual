# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from djangoplus.admin.views import public, index

urlpatterns = [
    url(r'^$', public),
    url(r'^admin/$', index),
    url(r'', include('djangoplus.admin.urls')),
]


